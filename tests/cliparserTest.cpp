/**
 * Copyright 2020 Łukasz Lewiński
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the * Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, * and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF * CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <climits>
#include <catch2/catch.hpp>

#include "cliparser.hpp"

TEST_CASE("Parse int") {
    using cliparser::Parser;
    Parser parser("Parser");
    parser.addArgument()
        .names("-a", "--arg")
        .description("Test Argument")
        .required(true);

    const char *argv[3] = {"cliparser", "-a", "123"};
    parser.parse(3, argv);

    REQUIRE(parser.get<uint16_t>("--arg") == 123);
}

TEST_CASE("3 int args") {
    using cliparser::Parser;
    Parser parser("Parser");
    parser.addArgument()
        .names("-a1", "--arg1")
        .description("Test Argument")
        .required(true);

    parser.addArgument()
        .names("-a2", "--arg2")
        .description("Test Argument")
        .required(true);

    parser.addArgument()
        .names("-a3", "--arg3")
        .description("Test Argument")
        .required(true);

    const char *argv[7] = {"cliparser", "-a1",    "123", "-a2",
                           "456",       "--arg3", "789"};
    parser.parse(7, argv);

    REQUIRE(parser.get<uint16_t>("--arg1") == 123);
    REQUIRE(parser.get<uint16_t>("--arg2") == 456);
    REQUIRE(parser.get<uint16_t>("--arg3") == 789);
}

TEST_CASE("Parse uint max") {
    using cliparser::Parser;
    Parser parser("Parser");
    parser.addArgument()
        .names("-a", "--arg")
        .description("Test Argument")
        .required(true);

    const char *argv[3] = {"cliparser", "-a", "4294967295"};
    parser.parse(3, argv);

    REQUIRE(parser.get<uint32_t>("--arg") == UINT_MAX);
}

TEST_CASE("Parse uint long max") {
    using cliparser::Parser;
    Parser parser("Parser");
    parser.addArgument()
        .names("-a", "--arg")
        .description("Test Argument")
        .required(true);

    const char *argv[3] = {"cliparser", "-a", "18446744073709551615"};
    parser.parse(3, argv);

    REQUIRE(parser.get<uint64_t>("--arg") == ULONG_MAX);
}

TEST_CASE("Parse string") {
    using cliparser::Parser;
    Parser parser("Parser");
    parser.addArgument()
        .names("-s", "--str")
        .description("Test Argument")
        .required(true);

    const char *argv[3] = {"cliparser", "-s", "string_arg"};
    parser.parse(3, argv);

    auto strMatchExpression = Catch::Equals("string_arg");
    REQUIRE_THAT(parser.get<std::string>("--str"), strMatchExpression);
}

TEST_CASE("Parse float") {
    using cliparser::Parser;
    Parser parser("Parser");
    parser.addArgument()
        .names("-a", "--arg")
        .description("Test Argument")
        .required(true);

    const char *argv[3] = {"cliparser", "-a", "1.0"};
    parser.parse(3, argv);

    REQUIRE(parser.get<float>("--arg") == 1.0f);
}

TEST_CASE("Parse double") {
    using cliparser::Parser;
    Parser parser("Parser");
    parser.addArgument()
        .names("-a", "--arg")
        .description("Test Argument")
        .required(true);

    const char *argv[3] = {"cliparser", "-a", "1.00000005"};
    parser.parse(3, argv);

    REQUIRE(parser.get<double>("--arg") == Approx(1.00000005f));
}

TEST_CASE("Wrong number of arguments") {
    using cliparser::Parser;
    Parser parser("Parser");
    parser.addArgument()
        .names("-s", "--str")
        .description("Test Argument")
        .required(true);

    const char *argv[3] = {"cliparser", "-a", "sample string"};

    REQUIRE_THROWS_WITH(parser.parse(3, argv), "Wrong number of arguments!");
}

TEST_CASE("No arguments but required one") {
    using cliparser::Parser;
    Parser parser("Parser");
    parser.addArgument()
        .names("-a", "--arg")
        .description("Test Argument")
        .required(true);

    const char *argv[1] = {"cliparser"};

    REQUIRE_THROWS_WITH(parser.parse(1, argv), "Wrong number of arguments!");
}

TEST_CASE("One arg without value") {
    using cliparser::Parser;
    Parser parser("Parser");
    parser.addArgument()
        .names("-a", "--arg")
        .description("Test Argument")
        .required(true);

    const char *argv[2] = {"cliparser", "-a"};

    REQUIRE_THROWS_WITH(parser.parse(2, argv), "Wrong number of arguments!");
}

TEST_CASE("Arg multi values") {
    using cliparser::Parser;
    Parser parser("Parser");
    parser.addArgument()
        .names("-a", "--arg")
        .description("Test Argument")
        .required(true)
        .numExpectedValues(3);

    const char *argv[5] = {"cliparser", "-a", "1", "2", "3"};
    parser.parse(5, argv);

    REQUIRE(parser.get<int>("--arg", 0) == 1);
    REQUIRE(parser.get<int>("--arg", 1) == 2);
    REQUIRE(parser.get<int>("--arg", 2) == 3);
}

TEST_CASE("Arg n values") {
    using cliparser::Parser;
    Parser parser("Parser");
    parser.addArgument()
        .names("-n", "--nums")
        .description("Test Argument")
        .required(true)
        .infiniteValues(true);

    const char *argv[8] = {"cliparser", "-n", "1", "2", "3", "4", "5", "6"};
    parser.parse(8, argv);

    auto argValues = parser.getValues<int>("--nums");

    REQUIRE(argValues.size() == 6);
    REQUIRE(argValues[0] == 1);
    REQUIRE(argValues[1] == 2);
    REQUIRE(argValues[2] == 3);
    REQUIRE(argValues[3] == 4);
    REQUIRE(argValues[4] == 5);
    REQUIRE(argValues[5] == 6);
}
