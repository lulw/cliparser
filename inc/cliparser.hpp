/**
 * Copyright 2020 Łukasz Lewiński
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the * Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, * and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF * CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef INC_CLIPARSER_HPP_
#define INC_CLIPARSER_HPP_

#include <algorithm>  // find_if
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <regex>

namespace cliparser {

class ArgParser;

const int ARG_START_INDEX = 1;
const uint8_t DEFAULT_ARG_VALUES_NUM = 1;

/**
 * Argumen - passed by command line interface.
 */
class Argument {
    friend class Parser;

 public:
    Argument()
        : m_numOfValues(DEFAULT_ARG_VALUES_NUM), m_argWasPassed(false),
          m_infiniteValues(false) {
    }

    Argument &names(std::string shortName, std::string longName) {
        m_shortName = shortName;
        m_longName = longName;
        return *this;
    }

    Argument &description(std::string description) {
        m_description = description;
        return *this;
    }

    Argument &required(bool required) {
        m_required = required;
        return *this;
    }

    Argument &numExpectedValues(uint8_t numOfValues) {
        m_numOfValues = numOfValues;
        return *this;
    }

    Argument &infiniteValues(bool haveInfiniteValues) {
        m_infiniteValues = haveInfiniteValues;
        return *this;
    }

    std::string getShortName() const {
        return m_shortName;
    }

    std::string getLongName() const {
        return m_longName;
    }

    std::string getDescription() const {
        return m_description;
    }

    bool getRequired() const {
        return m_required;
    }

    bool argWasPassed() const {
        return m_argWasPassed;
    }

 private:
    std::string m_shortName;
    std::string m_longName;
    std::string m_description;
    bool m_required;
    bool m_argWasPassed;
    uint8_t m_numOfValues;
    bool m_infiniteValues;

    // Parse result;
    std::vector<std::string> m_parseResult;

    template <typename T> T get(uint8_t index = 0) const noexcept(false) {
        if (index >= m_parseResult.size()) {
            throw "Wrong multi value index of argument!";
        }
        std::istringstream in(m_parseResult[index]);
        T t = T();
        in >> t >> std::ws;
        return t;
    }

    template <typename T> std::vector<T> getValues() noexcept(false) {
        std::vector<T> values;
        for (int index = 0; index < m_parseResult.size(); ++index) {
            values.push_back(get<T>(index));
        }
        return values;
    }

    bool isValid() {
        return m_numOfValues == m_parseResult.size();
    }
};

/**
 * ArgParser - main class providing builder interface to build cli arguments.
 */
class Parser {
 public:
    explicit Parser(std::string appName) : m_applicationName(appName) {
    }

    Argument &addArgument() {
        m_arguments.push_back({});
        return m_arguments.back();
    }

    std::string usage() {
        std::stringstream ss;
        ss << m_applicationName << "\n"
           << "Usage: program [options]\n"
           << "Options:\n";

        for (auto arg : m_arguments) {
            ss << "    " << arg.getShortName() << ", " << arg.getLongName()
               << std::setw(30) << arg.getDescription() << std::setw(10)
               << (arg.getRequired() ? "(Required)" : "") << "\n";
        }

        return ss.str();
    }

    void parse(int argc, const char **argv) noexcept(false) {
        // starting from 1 because 0 is executable name.
        if (argc > ARG_START_INDEX) {
            for (int i = ARG_START_INDEX; i < argc; i++) {
                std::string argName(argv[i]);
                Argument *argument = findArgByName(argName);
                if (argument && argument->m_infiniteValues) {
                    for (int j = i + 1; j < argc; j++) {
                        std::string value(argv[j]);
                        if (meetNextArg(value)) {
                            break;
                        }
                        argument->m_parseResult.push_back(argv[j]);
                    }
                    argument->m_numOfValues = argument->m_parseResult.size();
                    i += argument->m_numOfValues;
                } else if (argument && argc > i + argument->m_numOfValues) {
                    for (int j = i + 1; j <= i + argument->m_numOfValues; j++) {
                        argument->m_parseResult.push_back(argv[j]);
                    }
                    i += argument->m_numOfValues;
                }
            }
        }

        if (!validate()) {
            throw "Wrong number of arguments!";
        }
    }

    template <typename T>
    T get(const std::string &name, uint8_t index = 0) noexcept(false) {
        Argument *argument = findArgByName(name);
        if (argument) {
            return argument->get<T>(index);
        } else {
            throw "Arg not found!";
        }
        return T();
    }

    template <typename T>
    std::vector<T> getValues(const std::string &name) noexcept(false) {
        Argument *argument = findArgByName(name);
        if (argument) {
            return argument->getValues<T>();
        } else {
            throw "Arg not found!";
        }
        return std::vector<T>();
    }

 private:
    std::string m_applicationName;
    std::vector<Argument> m_arguments;

    bool meetNextArg(const std::string &arg) {
        auto nextArgMeet = false;
        std::regex pattern("(-|--)(\\w+)", std::regex_constants::ECMAScript |
                                               std::regex_constants::icase);
        if (std::regex_search(arg, pattern)) {
            nextArgMeet = true;
        }
        return nextArgMeet;
    }

    bool validate() {
        bool valid = true;
        for (auto &a : m_arguments) {
            if (a.getRequired() || a.argWasPassed()) {
                valid &= a.isValid();
            }
        }
        return valid;
    }

    Argument *findArgByName(const std::string &name) {
        auto found = std::find_if(
            m_arguments.begin(), m_arguments.end(), [&name](const Argument &a) {
                return a.getShortName() == name || a.getLongName() == name;
            });

        if (found != m_arguments.end()) {
            return &(*found);
        }

        return nullptr;
    }
};

}  // namespace cliparser

#endif  // INC_CLIPARSER_HPP_

